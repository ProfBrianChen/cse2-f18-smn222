//Sofia Nachmias
//September 25, 2018
//CSE 002 - Craps If

import java.util.Scanner; //imports the scanner class  

public class CrapsIf{
  //main method required for every Java program
  public static void main (String[]args) {
    Scanner myScanner;
    myScanner = new Scanner(System.in);
    
    int craps1 = (int)(Math.random()*6)+1; //fixes the range of the integers
    int craps2 = (int)(Math.random()*6)+1; //fixes the range of the integers
    String slang = ""; //a String corresponding to the slang terminology 
    
    System.out.println("Would you like to randomly cast dice? (Type 1 for Yes or 2 for No)"); //prompts the user for if they want a random or selected roll
    int answer = myScanner.nextInt(); //accepts user input
    
    myScanner.nextLine();
    
    //assgins the randomly generated number to a slang term 
    if (answer == 1){
      System.out.println("Craps1: " + craps1);
      System.out.println("Craps2: " + craps2);
   
    if (craps1==1 && craps2==1){
      slang = "Snake Eyes";
    }
    else if (craps1==1 && craps2==2){
      slang = "Ace Deuce";
    }
    else if (craps1==1 && craps2==3){
      slang = "Easy Four";
    }
    else if (craps1==1 && craps2==4){
      slang = "Fever Five";
    }
    else if (craps1==1 && craps2==5){
      slang = "Easy Six";
    }
    else if (craps1==1 && craps2==6){
      slang = "Seven out";
    }
    else if (craps1==2 && craps2==2){
      slang = "Hard Four";
    }
    else if (craps1==2 && craps2==3){
      slang = "Fever Five";
    }
    else if (craps1==2 && craps2==4){
      slang = "Easy Six";
    }
    else if (craps1==2 && craps2==5){
      slang = "Seven out";
    }
    else if (craps1==2 && craps2==6){
      slang = "Easy Eight";
    }
    else if (craps1==3 && craps2==3){
      slang = "Hard Six";
    }
    else if (craps1==3 && craps2==4) {
      slang = "Seven out";
    }    
    else if (craps1==3 && craps2==5){
      slang = "Easy eight";
    }
    else if (craps1==3 && craps2==6){
      slang = "Nine";
    }
    else if (craps1==4 && craps2==4){
      slang = "Hard Eight";
    }
    else if (craps1==4 && craps2==5){
      slang = "Nine";
    }
    else if (craps1==4 && craps2==6){
      slang = "Easy Ten";
    }
    else if (craps1==5 && craps2==5){
      slang = "Hard Ten";
    }
    else if (craps1==5 && craps2==6){
      slang = "Yo-leven";
    }
    else if (craps1==6 && craps2==6){
      slang = "Boxcars";
    }
  }
    
    //assgins the user selected integers to a slang term 
   else if (answer == 2){
     System.out.println("Enter one integer from 1-6");
     int number1 = myScanner.nextInt();
     System.out.println("Enter another integer from 1-6");
     int number2 = myScanner.nextInt();
   
   if (number1==1 && number2==1){
      slang = "Snake Eyes";
    }
    else if (number1==1 && number2==2){
      slang = "Ace Deuce";
    }
    else if (number1==1 && number2==3){
      slang = "Easy Four";
    }
    else if (number1==1 && number2==4){
      slang = "Fever Five";
    }
    else if (number1==1 && number2==5){
      slang = "Easy Six";
    }
    else if (number1==1 && number2==6){
      slang = "Seven out";
    }
    else if (number1==2 && number2==2){
      slang = "Hard Four";
    }
    else if (number1==2 && number2==3){
      slang = "Fever Five";
    }
    else if (number1==2 && number2==4){
      slang = "Easy Six";
    }
    else if (number1==2 && number2==5){
      slang = "Seven out";
    }
    else if (number1==2 && number2==6){
      slang = "Easy Eight";
    }
    else if (number1==3 && number2==3){
      slang = "Hard Six";
    }
    else if (number1==3 && number2==4) {
      slang = "Seven out";
    }    
    else if (number1==3 && number2==5){
      slang = "Easy eight";
    }
    else if (number1==3 && number2==6){
      slang = "Nine";
    }
    else if (number1==4 && number2==4){
      slang = "Hard Eight";
    }
    else if (number1==4 && number2==5){
      slang = "Nine";
    }
    else if (number1==4 && number2==6){
      slang = "Easy Ten";
    }
    else if (number1==5 && number2==5){
      slang = "Hard Ten";
    }
    else if (number1==5 && number2==6){
      slang = "Yo-leven";
    }
    else if (number1==6 && number2==6){
      slang = "Boxcars";
    }
   }
    
    System.out.println("The slang terminology for your roll is: " + slang); //prints out the slang terminology for the roll
  }
}