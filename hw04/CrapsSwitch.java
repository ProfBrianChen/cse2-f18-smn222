//Sofia Nachmias
//September 25, 2018
//CSE 002 - Craps Switch

import java.util.Scanner; //imports the scanner class


public class CrapsSwitch{
  //main method required for every Java program
  public static void main (String[] args){
   
   Scanner myScanner;
   myScanner = new Scanner(System.in);
    
    int craps1 = (int)(Math.random()*6)+1; //fixes the range of the integers
    int craps2 = (int)(Math.random()*6)+1; //fixes the range of the integers
    String slang = ""; //a String corresponding to the slang terminology 
    
   System.out.println("Would you like to randomly cast dice? (Type 1 for Yes or 2 for No)"); //prompts the user for if they want a random or selected roll
   int answer = myScanner.nextInt(); //accepts user input
   
   myScanner.nextLine();
   
    //assgins the randomly generated number to a slang term 
   if (answer == 1){
      System.out.println("Craps1: " + craps1);
      System.out.println("Craps2: " + craps2);
   
     int number = ((craps1+craps2)-1);
   switch(number){
     case 1: 
       slang = "Snake Eyes";
       break;
     case 2:
       slang = "Ace Deuce";
       break;
     case 3:
       slang = "Easy Four";
       break;
     case 4:
       slang = "Fever Five";
       break;
     case 5:
       slang = "Easy Six";
       break;
     case 6:
       slang = "Seven out";
       break;
     case 7:
       slang = "Hard Four";
       break;
     case 8:
       slang = "Easy Eight";
       break;
     case 9:
       slang = "Hard Eight";
       break;
     case 10:
       slang = "Nine";
       break;
     case 11:
       slang = "Easy Ten";
       break;
     case 12:
       slang = "Hard Ten";
       break;
     case 13:
       slang = "Yo-leven";
       break;
     case 14:
       slang = "Boxcars";
       break;     
   }
   }    
   
      //assgins the user selected integers to a slang term
   else if (answer == 2){
     System.out.println("Enter one integer from 1-6");
     int number1 = myScanner.nextInt();
     System.out.println("Enter another integer from 1-6");
     int number2 = myScanner.nextInt();
     
     int number = ((number1+number2));
   switch(number){
     case 1: 
       number = (number);
       slang = "Snake Eyes";
       break;
     case 2:
       slang = "Ace Deuce";
       break;
     case 3:
       slang = "Easy Four";
       break;
     case 4:
       slang = "Fever Five";
       break;
     case 5:
       slang = "Easy Six";
       break;
     case 6:
       slang = "Seven out";
       break;
     case 7:
       slang = "Hard Four";
       break;
     case 8:
       slang = "Easy Eight";
       break;
     case 9:
       slang = "Hard Eight";
       break;
     case 10:
       slang = "Nine";
       break;
     case 11:
       slang = "Easy Ten";
       break;
     case 12:
       slang = "Hard Ten";
       break;
     case 13:
       slang = "Yo-leven";
       break;
     case 14:
       slang = "Boxcars";
       break;     
   }
   }
    
    System.out.println("The slang terminology for your roll is: " + slang); //prints out the slang terminology for the roll
    
  }
}