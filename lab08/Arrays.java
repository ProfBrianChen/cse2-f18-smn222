//Sofia Nachmias
//CSE002 Lab08
//November 8, 2018

public class Arrays{
  
  public static void main(String[] args){
    
    int[] numbers = new int[100]; //creates the numbers array and sets it to a size of 100
    
    //prints out a list of 100 random numbers from 0-99
    for (int i = 0; i < numbers.length; i++){
      numbers[i] = (int)(Math.random() * 100);
      System.out.print(numbers[i] + " ");
    }
    System.out.println();
    
    int x = 0;
    int[] occurrences = new int[100]; //creates the occurrences array and sets it to a size of 100
    
    //calculates how many times each number occurs in the list
    for(int i = 0; i < numbers.length; i++){
      x = numbers[i];
      occurrences[x]++;
    }
    
    //prints out the number of times each number occurs in the list
    for (int i = 0; i < occurrences.length; i++){
     if (occurrences[i] == 1){
        System.out.println( i + " occurs " + occurrences[i] + " time");
      }
      else if (occurrences[i] >= 2){
        System.out.println( i + " occurs " + occurrences[i] + " times");
      }
    }
  }
}