//Sofia Nachmias
//CSE002 - HW06
//October 23, 2018

import java.util.Scanner; //imports the scanner class

public class EncryptedX{ 
 //main method required for every Java program 
  public static void main (String[]args){
    
    Scanner myScanner = new Scanner(System.in); //creates the new scanner
    
    int i, j; //declares variables i and j as integers
    
    //asks the user to input an integer between 0 and 100
    System.out.println("Input an integer between 0 and 100.");
      boolean number = myScanner.hasNextInt();  
    
    //checks to see if the user entered an integer 
      while((number == false)){
        String junk = myScanner.next();
        System.out.println("Input an integer between 0 and 100.");
        number = myScanner.hasNextInt();
      }
       
      int num = myScanner.nextInt();
      
      //checks to see if the integer the user entered is within the boundaries
      while((num < 0) || (num > 100)){
        int junk = myScanner.nextInt();
        System.out.println("Input an integer between 0 and 100.");
        num = myScanner.nextInt();
      }
    
    //creates the box pattern with the secret message X
    for(i=0; i<=num; i++){ //main component that creates a box pattern 
      for(j=0; j<=num; j++){
        if (j==i){ //main component that creates the X pattern
          System.out.print(" ");
        } else if (j==num - i){
          System.out.print(" ");
        } else {
          System.out.print("*");
        }
      }
      System.out.println();
    }
    
  }
} 
