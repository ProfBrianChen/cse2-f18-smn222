// Sofia Nachmias
// September 13, 2018
// CSE 02 - Check - uses the Scanner class to obtain from the user the original cost of the check, the percentage tip they wish to pay, the number of ways the check will be split, and how much each person in the group needs to spend in order to pay the check.

import java.util.Scanner; //imports the Scanner class

public class Check{
  // main method required for every Java program
  public static void main (String[] args) {
    
    Scanner myScanner = new Scanner (System.in);
    
    System.out.print("Enter the original cost of the check in the form xx.xx:"); //prompts the user for the original cost of the check 
    double checkCost = myScanner.nextDouble(); //accepts user input
    System.out.print("Enter the percentage tip that you wish to pay as a whole number (in the form xx):"); //prompts the user for the tip percentage that they wish to pay 
    double tipPercent = myScanner.nextDouble(); //accepts user input
    tipPercent /= 100; //allows us to convert the percentage into a decimal value
    System.out.print("Enter the number of people who went out to dinner: "); //promts the user for the number of people that went to dinner
    int numPeople = myScanner.nextInt(); //accepts user input
    
    //outputs the amount that each member of the group needs to spend in order to pay the check 
    double totalCost;
    double costPerPerson;
    int dollars, //whole dollar amount of cost 
          dimes, pennies; //for storing digits to the right of the decimal point for the cost 
    totalCost = checkCost * (1 + tipPercent);
    costPerPerson = totalCost / numPeople; 
    //get the whole amount, dropping decimal fraction 
    dollars = (int) costPerPerson; 
    //get dimes amount, where the % (mod) operator reutrns the remainder after the division 
    dimes = (int) (costPerPerson * 10) % 10;
    pennies = (int) (costPerPerson * 100) % 10;
    System.out.println("Each person in the group owes $" + dollars + '.' + dimes + pennies); //prints out the exact price each person in the group has to pay for the dinner 
    
    
    
  }  //end of main method   
} //end of class
