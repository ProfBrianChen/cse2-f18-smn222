// Sofia Nachmias
// September 6, 2018
// CSE 02 - Cyclometer 

public class Cyclometer{
  // main method required for every Java program
  public static void main (String[] args) {
    
    int secsTrip1 = 480; //number of seconds for trip 1 
    int secsTrip2 = 3220; // number of seconds for trip 2 
    int countsTrip1 = 1561; // number of counts (rotations) for trip 1 
    int countsTrip2 = 9037; // number of counts (rotations) for trip 2 
    
    double wheelDiameter = 27.0, // the diameter of the bicycle wheel 
    PI = 3.14159, //sets the numerical value of pi 
    feetPerMile = 5280, // sets the numerical value of how many feet are in a mile
    inchesPerFoot = 12, // sets the numerical value for how many inches are in a foot 
    secondsPerMinute = 60; // sets the numerical value for how many seconds are in a minute
    double distanceTrip1, distanceTrip2, totalDistance; // sets the variables as double values
    
    System.out.println("Trip 1 took " + (secsTrip1/secondsPerMinute) + " minutes and had " + countsTrip1 + " counts.");
    System.out.println("Trip 2 took " + (secsTrip2/secondsPerMinute) + " minutes and had " + countsTrip2 + " counts.");
    // prints two lines that descives the number of sceonds (converted to minutes) and the counts for each trip. 
    
    distanceTrip1 = countsTrip1*wheelDiameter*PI;
    // above gives distance in inches (for each count, a rotation of the wheel travels the diameter in inches times PI)
    distanceTrip1 /= inchesPerFoot*feetPerMile;
    // gives distance in miles
    distanceTrip2 = countsTrip2*wheelDiameter*PI/inchesPerFoot/feetPerMile;
    totalDistance = distanceTrip1+distanceTrip2;
    // calculates the toal distance traveled
    
    // print out the output data
    System.out.println("Trip 1 was " + distanceTrip1 + " miles");
    System.out.println("Trip 2 was " + distanceTrip2 + " miles");
    System.out.println("The total distance was " + totalDistance + " miles");

  } // end of main method 
} // end of class