//Sofia Nachmias
//CSE002 Lab09
//November 22, 2019

public class Passing{

  public static void main(String[]args){
    int[] x = new int[8]; //declares a literal integer array of length 8
    //defines the array
    x[0] = 0;
    x[1] = 1;
    x[2] = 2;
    x[3] = 3;
    x[4] = 4;
    x[5] = 5;
    x[6] = 6;
    x[7] = 7;
    //make two copies of it using the copy() method
    int[] array0 = new int[8];
    array0 = copy(x);
    int[] array1 = new int[8];
    array1 = copy(x);
    int[] array2 = new int[8];
    array2 = copy(x);
    int[] array3 = new int[8];
    
    inverter(array0); //Pass array0 to inverter()
    inverter2(array1); //Pass array1 to inverter2()
    array3 = inverter2(array2); //Pass array2 to inverter2() and assign the output to array3
    print(array3); //Print out array3 with print()
    System.out.println();
    print(array0);
    print(array1);
    print(array2);
    print(array3);
  }

  //Creates a method called copy(), which accepts an integer array as input, and returns an integer array as output
  public static int[] copy(int[] number){
    int[] y = new int[number.length]; //declare and allocate a new integer array that is the same length as the input
    //copy each member of the input to the same member in the new array
    for (int i = 0; i < number.length; i++){
      y[i] = number[i];
    }
    return y; //return the new array as output
  }
  
  //Creates a method, inverter(), which reverses the order of an array by accepting an array of integers as input
  public static void inverter(int[] inputs){
    for (int j = 0; j < inputs.length/2; j++){
      int temp = inputs[j];
      inputs[j] = inputs[inputs.length-j-1];
      inputs[inputs.length-j-1] = temp;
      print(inputs);
      System.out.println();
    }
  }
  
  //Creates a method, inverter2(), which uses copy() to make a copy of the input array and uses a copy of the code from inverter() to invert the members of the copy
  public static int[] inverter2(int[] value){
    copy(value);
    int[] temp = new int[8];
    temp = copy(value);
    inverter(temp);
    print(value);
    System.out.println();
    return temp; //returns the copy as output
  }
  
  //Creates a method called print() which accepts any integer array as input, and returns nothing
  public static void print(int[] accept){
    for (int m = 0; m < accept.length; m++){
      System.out.print(accept[m] + " ");
    }
    System.out.println();
    return; 
  }
}