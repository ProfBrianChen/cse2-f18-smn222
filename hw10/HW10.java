//Sofia Nachmias
//CSE 002 HW 10
//December 4, 2018

import java.util.Scanner;

public class HW10{
  
  public static void main(String[] args) {
    Scanner myScanner = new Scanner(System.in);
    int row;
    int column;
    char player = 'O'; //sets the first player to O
    
    //create 2 dimensional array for tic tac toe board
    char[][] board = new char[3][3];
    char ch = '1';
    for (int i = 0; i < 3; i++){
      for (int j = 0; j < 3; j++) {
        board[i][j] = ch++;
      }
    }
    createBoard(board); 
    while(!winner(board) == true){
      
      //prompts the player to enter the placement of the token
      System.out.println(player + "'s turn. Enter a row and column (0, 1, or 2)."); 
      System.out.print("Row: ");
      row = myScanner.nextInt();
      System.out.println();
      System.out.print("Column: ");
      column = myScanner.nextInt();
      
      //determines if the space is occupied
      while (board[row][column] == 'X' || board[row][column] == 'O') {
        System.out.println("This spot is occupied. Please try again");
      }
      //place the X in the designated space
      board[row][column] = player;
      createBoard(board);
      
      //determine winner
      if (winner(board)){
        System.out.println("Player " + player + " is the winner!");
      }
      
      //swap players after each go.
      if (player == 'X') {
        player = 'O';
      }
      else {
        player = 'X';
      }
      
      //determine if the result is a draw
      if (winner(board) == false && !hasFreeSpace(board)) {
        System.out.println("The game is a draw. Please try again.");
      }
    }
  }
  
  //method used to display the created TicTacToe board
  public static void createBoard(char[][] board) {
    for (int i = 0; i < board.length; i++) {
      for (int j = 0; j < board[i].length; j++) {
        if (j == board[i].length - 1) System.out.print(board[i][j]);
        else System.out.print( board[i][j] + "  ");
      }
      System.out.println();
    }
  }
  
  //Determines whether the board is completely occupied by X and O characters
  public static boolean hasFreeSpace(char[][] board){
    for (int i = 0; i< board.length; i++) {
      for (int j = 0; j < board[0].length; j++) {
        if (board[i][j] != 'O' && board[i][j] != 'X') {
          return true;
        }
      }
    }
    return false;
  }
  
  //method to determine whether there is a winner
  public static boolean winner(char[][] board){
    return isHorizontalWin(board) || isVerticalWin(board) || isDiagonalWin(board);
  }
  
  // method to determine if there is a winner by checking each row for consecutive matching tokens
  private static boolean isHorizontalWin(char[][] board) {
    for(int row = 0; row < board.length; row++){
      if(isWin(board[row]))
        return true;
    }
    return false;
  }
  
  
  //method to determine whether all of the buttons in the specified array have the same text and that the text is not empty string
  private static boolean isWin(char[] lineToProcess) {
    boolean foundWin = true;
    char prevChar = '-';
    for(char character: lineToProcess) {
      if(prevChar == '-')
        prevChar = character;
      if ('O' != character && 'X' != character) {
        foundWin = false;
        break;
      } else if (prevChar != character) {
        foundWin = false;
        break;
      }
    }
    return foundWin;
  }
  
  //method to determine whether there is a winner by checking column for consecutive matching tokens
  private static boolean isVerticalWin(char[][] board) {
    char[] column = null;
    for(int col = 0; col < board[0].length; col++){
      column = new char[board[0].length]; 
      for(int row = 0; row < column.length; row++){
        column[row] = board[row][col];
      }
      if(isWin(column))
        return true;
    }
    return false;
  }
  
  //method to determine if there is a winner by checking each diagonal for consecutive matching tokens
  private static boolean isDiagonalWin(char[][] board) {
    int row = 0, col = 0;
    int cols = board.length;
    int rows = board[0].length; 
    
    //creates a one-dimensional array to represent the diagonal
    int size = rows < cols ? rows : cols;
    char[] diagonal = new char[size];
    
    while (row < rows && col < cols) {
      diagonal[col] = board[row][col];
      row++;
      col++;
    }
    
    if (isWin(diagonal)) {
      return true;
    }
    
    row = rows - 1;
    col = 0;
    diagonal = new char[size];
    
    while (row >=0 && col < cols) {
      diagonal[col] = board[row][col];
      row--;
      col++;
    }
    return isWin(diagonal);
  }
}