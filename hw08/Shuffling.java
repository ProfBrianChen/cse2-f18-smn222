//Sofia Nachmias
//CSE002 HW08
//November 15, 2018

import java.util.Scanner;
import java.util.Random;
import java.util.concurrent.ThreadLocalRandom;
import java.util.Arrays;

public class Shuffling{ 
public static void main(String[] args) { 
Scanner scan = new Scanner(System.in); 
String[] suitNames={"C","H","S","D"};   //represents the suits clubs, hearts, spades and diamonds
String[] rankNames={"2", "3", "4", "5", "6", "7", "8", "9", "10", "J", "Q","K","A"}; //the number values and symbols for the face cards
String[] cards = new String[52]; //creates 52 numbers
String[] hand = new String[5]; //creates a hand of 5 cards
int numCards = 0; 
int temp = 1; //temporary variable that will change based on whether the user wants another hand
int index = 51;
printArray(cards); 
shuffle(cards); 
while(temp == 1){ //starts the while-loop that runs until the user enters something other than 0
   getHand(cards,index,numCards);//prints the value from the getHand method
   index = index-numCards;
   System.out.println();
   System.out.println("Enter a 1 if you want another hand drawn"); //asks the user if they want another hand
   temp = scan.nextInt(); //collects the value the user enters
}  
  } 
  
public static void printArray(String[] cards){//This method prints out the deck of cards
  String[] suitNames={"C","H","S","D"};   //represent the suits clubs, hearts, spades and diamonds 
  String[] rankNames={"2", "3", "4", "5", "6", "7", "8", "9", "10", "J", "Q","K","A"}; //the number values and symbols for the face cards
  String[] hand = new String[5]; //creates a hand of 5 cards
  int numCards = 0; 
  int again = 1; //temporary variable that will change based on whether the user wants another hand
  int index = 51;
  for (int i=0; i<52; i++){ //for loop that creates the number and letter pairs for the cards
  cards[i]=rankNames[i%13]+suitNames[i/13]; 
  System.out.print(cards[i]+" "); //prints the cards and a space
  } 
  System.out.println();//prints a new line
 }
public static void shuffle(String [] cards){//This method shuffles the deck of cards printed in the method printArray 
  String[] randomString = new String[52];//creates a new string that holds the entire deck
  String printingRandom = "";//an empty string that holds what gets printed
  Random rnd = ThreadLocalRandom.current();//creates a random thread
  for (int l = cards.length - 1; l > 0; l--){
    int index = rnd.nextInt(l + 1);//changes index
    String CardA = cards[index];//creates a string called CardA
    cards[index] = cards[l];//sets the two arrays equal
    cards[l] = CardA;
    randomString = cards;//makes the random string equal the original string
    printingRandom = Arrays.toString(randomString);//changes the array to a string
  }
  System.out.println("Shuffled");
  System.out.println(printingRandom);//prints the shuffled list
  System.out.println();
}
public static void getHand(String[] cards, int index, int numCards){//creates a method that gets a hand of 5 cards
  String[] hand = new String[5];//creates a string hand that has room for 5 cards
  String[] randomString = cards;//sets randomString to cards
  randomString = cards;
  System.out.println("Hand");
  for(int m=0; m<5; m++){//finds the first 5 cards
   System.out.print(randomString[m] + ", ");//prints the hand
  }
  System.out.println();
}
}
