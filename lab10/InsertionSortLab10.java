//Sofia Nachmias
//CSE002 Lab10
//December 6, 2018

import java.util.Arrays;

//code from Professor Carr lab
public class InsertionSortLab10{
  
  public static void main (String[]args){
    int[] myArrayBest = {1,2,3,4,5,6,7,8,9};
    int[] myArrayWorst = {9,8,7,6,5,4,3,2,1};
    int iterBest = insertionSort(myArrayBest);
    System.out.println("The total number of operations performed on the sorted array: " + iterBest);
    int iterWorst = insertionSort(myArrayWorst);
    System.out.println("The total number of operations performed on the reverse sorted array: " + iterWorst);
  }
  
  //the method for sorting the numbers
  public static int insertionSort (int[] list){
    //prints the initial array 
    System.out.println(Arrays.toString(list));
    //initialize counter for iterations
    int iterations = 0;
    int temp;
    
    for (int i = 1; i < list.length; i++){
      iterations++;
      for (int j = i; j > 0; j--){
        if (list[j] < list[j-1]){
          temp = list[j];
          list[j] = list[j-1];
          list[j-1] = temp;
          iterations++;
        }
        else {
          break;
        }
      }
      System.out.println(Arrays.toString(list));
    }
    return iterations;
  }
}