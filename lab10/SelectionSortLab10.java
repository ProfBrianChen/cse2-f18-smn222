//Sofia Nachmias
//CSE002 Lab10
//December 6, 2018

import java.util.Arrays;

//code from Professor Carr on lab
public class SelectionSortLab10{
  public static void main (String[]args){
    int[] myArrayBest = {1,2,3,4,5,6,7,8,9};
    int[]myArrayWorst = {9,8,7,6,5,4,3,2,1};
    int iterBest = selectionSort(myArrayBest);
    System.out.println("The total number of operations performed on the sorted array: " + iterBest);
    int iterWorst = selectionSort(myArrayWorst);
    System.out.println("The total number of operations performed on the reverse sorted array: " + iterWorst);
  }
  
  //The method for sorting the numbers
  public static int selectionSort (int[] list){
    //Prints out initial array
    System.out.println(Arrays.toString(list));
    //initialize counter for iterations
    int iterations = 0;
    int temp;
    
    for (int i = 0; i < list.length - 1; i++){
      //update the iterations counter
      iterations++;
      
      //Step 1: find the minimum in the list
      int currentMin = list[i];
      int currentMinIndex = i;
      for (int j = i+1; j < list.length; j++){
        if (list[i] < currentMin){
          currentMin = list[i];
        }
        iterations++;
      }
      
      //Step 2: swap list[i] with the minimum value found above
      if (currentMinIndex != i){
        temp = list[i];
        list[i] = currentMin;
        currentMin = temp;
        currentMinIndex = i;
        iterations++;
      }
      System.out.println(Arrays.toString(list));
    }
    return iterations;
  }
}