//Sofia Nachmias
//CSE002 HW09
//November 27, 2018

import java.util.Scanner; //imports the scanner class
import java.util.Arrays; //imports the arrays class
import java.util.Random; //imports the random class
import java.util.concurrent.ThreadLocalRandom; //imports the thread local random class

public class CSE2Linear{
  public static void main(String args[]){
    Scanner myScanner = new Scanner(System.in); //creates the new scanner
    int[] grade = new int[15];
    
    String answer="";
    
    ////asks the user to input their grades
    for (int i = 0; i < grade.length; i++){
      System.out.println("Enter 15 ascending ints for final grades in CSE2 (0-100):");
      grade[i] = myScanner.nextInt();
    }
    
    do{
      System.out.println("List of grades" + Arrays.toString(grade));
      System.out.println("Enter a grade to search for:");
      int score = myScanner.nextInt();
      
      System.out.println(binarySearch(grade, score)); //use binary search to find the entered grade
      scramble(grade); //scramble the sorted array randomly, and print out the scrambled array
      System.out.println("Enter another grade to be searched for:");
      int linearGrade = myScanner.nextInt();
      
      System.out.println(linearSearch(grade, linearGrade)); //use linear search to find the entered grade
      
      System.out.print("Go again? Enter 'yes', anything else to quit-");
      answer=myScanner.next();
    }
    while(answer.equals("yes"));
  }
  
  //binary search from Professor Carr
  public static String binarySearch(int[] list, int grade){
    
    int low = 0;
    int high = list.length-1;
    int times = 1;
    
    while (high >= low) {
      int mid = (low + high) / 2;
      
      if (grade < list[mid]) {
        high = mid - 1;
        times++;
      } 
      else if (grade > list[mid]) {
        low = mid + 1;
        times++;
      } 
      else {
        return "The value was found at index: " + mid +" using " + times + " iterations"; //This will return a message with the index of the grade
      }
    }
    return "The grade was not found in the list with " + times + " iterations."; //This will return if the grade was not in the index
  }
  
  public static void scramble(int[] list){
    int[] randomString = new int[15];//This creates a new int array with enough space for all the grades
    String printingRandom = "";//This is an empty string to hold what gets printed
    Random rnd = ThreadLocalRandom.current();//This creates a random thread
    for (int l = list.length - 1; l > 0; l--){
      int index = rnd.nextInt(l + 1);//This changes index
      int rand = list[index];//This creates a int called rand
      list[index] = list[l];//this sets the two arrays equal
      list[l] = rand;
      randomString = list;//this makes random string equal to the original string
      printingRandom = Arrays.toString(randomString);//This changes the array to a printable string
    }
    System.out.println("Scrambled: " + printingRandom);//This prints the shuffled list
  }
  
  public static String linearSearch(int[] list, int linearGrade){
    int count = 0;
    for (int i = 0; i < list.length; ++i) {
      if (linearGrade == list[i]) {
        return "The grade was found at index:" + i + " using " + count + " iterations";
      }
      else{
        count++;
      }
    }     
    return "The grade was not found in the list with " + count + " iterations."; //This will return if the grade was not found 
  }
}