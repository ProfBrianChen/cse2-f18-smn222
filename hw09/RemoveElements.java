//Sofia Nachmias
//CSE 002 HW09
//November 27, 2018

import java.util.Scanner; //imports the scanner class
import java.util.Arrays; //imports the arrays class
import java.util.Random; //imports the random class

public class RemoveElements{
  
  public static void main(String [] arg){
    
    Scanner scan=new Scanner(System.in);
    
    int num[]=new int[10];
    int newArray1[];
    int newArray2[];
    int index,target;
    String answer="";
    
    do{
      System.out.println("Random input 10 ints [0-9]");
      num = randomInput();
      String out = "The original array is: ";
      out += listArray(num);
      System.out.println(out);
      
      System.out.print("Enter the index ");
      index = scan.nextInt();
      //checks the pos index to make sure it is not out of bounds on the array
      while((index < 0) || (index > num.length-1)){
        int junk = scan.nextInt();
        System.out.println("Integer must be between 0 and 9.");
        index = scan.nextInt();
      }
      newArray1 = delete(num,index);
      System.out.println("Index " + index + " has been removed."); //tells user that the index was removed
      String out1="The output array is ";
      out1+=listArray(newArray1); //return a string of the form "{2, 3, -9}"  
      System.out.println(out1);
      
      
      System.out.print("Enter the target value ");
      target = scan.nextInt();
      newArray2 = remove(num,target);
      String out2="The output array is ";
      out2+=listArray(newArray2); //return a string of the form "{2, 3, -9}"  
      System.out.println(out2);
      
      System.out.print("Go again? Enter 'y' or 'Y', anything else to quit-");
      answer=scan.next();
    }
    while(answer.equals("Y") || answer.equals("y"));
  }
  
  public static String listArray(int num[]){
    String out="{";
    for(int j=0;j<num.length;j++){
      if(j>0){
        out+=", ";
      }
      out+=num[j];
    }
    out+="} ";
    return out;
  }
  
  //The randomInput() method generates an array of 10 random integers between 0 to 9. Implement randomInput so that it fills the array with random integers and returns the filled array. 
  public static int[] randomInput(){
    //creates an array of 10 random integers 0-9
    int[] random = new int[10];
    for (int i = 0; i < random.length; i++){
      random[i] = (int)(Math.random()*10 + 1);
    }
    System.out.println(Arrays.toString(random)); //fills the array with random integers
    return random; //returns the filled array
  }
  
  //The method delete(list,pos) takes, as input, an integer array called list and an integer called pos. It should create a new array that has one member fewer than list, and be composed of all of the same members except the member in the position pos. 
  public static int[] delete(int[] list,int pos){
    if (pos < 0 || pos >= list.length) { 
      return list; 
    } 
    
    int[] second = new int[list.length - 1]; //reate a new array that has one member fewer than list
    
    //composes list of all of the same members except the member in the position pos
    for (int i = 0, j = 0; i < list.length; i++) { 
      if (i == pos) { 
        continue; 
      } 
      second[j++] = list[i]; 
    } 
    return second;
  }
  
  //Method remove(list,target) deletes all the elements that are equal to target, returning a new list without all those new elements.
  public static int[] remove (int[] list, int target){
    //determines if the target value was found in the list and tells user
    for(int s: list){
      if (s == target){
        System.out.println("Element " + target + " has been found.");
        break;
      }
      else{
        System.out.println("Element " + target + " was not found.");
      }
      break;
    }
    
    //deletes all of the elements equal to the target
    int occurrences = 0;
    for (int i = 0; i < list.length; i++){
      if (list[i] != target){
        list[occurrences++] = list[i];
      }
    }
    return Arrays.copyOf(list, occurrences); //returns the new list without the target elements
  }
}