//Sofia Nachmias
//CSE002 Lab 07
//Novermber 8, 2018

import java.util.Random; //imports the Random class
import java.util.Scanner; //imports the Scanner class

public class MethodsParagraph{
  //main method required for every Java program 
  public static void main(String[]args){

    Scanner myScanner = new Scanner(System.in); //creates the new scanner
    
    boolean yes = true; //declares the boolean value yes as false
    
    System.out.println("The quick brown fox passed the lazy dog."); //prints out the thesis sentence
   
    //Creates a loop that, at each iteration, assembles the words into a string and prints it
    while (yes){
      Random randomGenerator = new Random(); //creats the new random generator
      int randomInt = randomGenerator.nextInt(10); //generates a random integer less than 10

      String action = actionSent(randomInt);
      String conclusion = conclusionSent(randomInt);
      
      //asks the user if they want to continue generating sentences and accepts the users input
      System.out.println("Do you want to generate a paragraph?");
      String input = myScanner.next();
      
      //checks user input 
      if (input.equals("yes")){
        yes = true;
      }
      else if (input.equals("no")){
        yes = false;
      }
      else if (input.equals(" ")){
        System.out.println("Please input yes or no.");
      }
      
      //check for yes
      if (yes) {
        System.out.println("The quick brown fox passed the lazy dog."); //prints out the thesis sentence
        System.out.println(action);
        System.out.println(conclusion);
      }
      else {
        System.exit(0);
      }
      
    }
  }
  
//method that outptuts an action sentence for each of the random integers generated
  public static String actionSent(int randomInt){
    String actionSentence = "";
    switch (randomInt){
      case 0: actionSentence = "This fox was particularly mean to passing warplanes.";
      break;
      case 1: actionSentence = "It used afterburners to spew plant matter at the fearsome cat.";
      break;
      case 2: actionSentence = "This fox was running in the woods.";
      break;
      case 3: actionSentence = "It was particularly mean to the other animals.";
      break;
      case 4: actionSentence = "The fox ate the leaves.";
      break;
      case 5: actionSentence = "The fox was playing with the other foxes.";
      break;
      case 6: actionSentence = "This fox threw its food in the bush.";
      break;
      case 7: actionSentence = "It fought the dog.";
      break;
      case 8: actionSentence = "The fox was attacking everything in sight.";
      break;
      case 9: actionSentence = "It tried to be by itself all of the time.";
      break;
      }
    return actionSentence;
  }
  
//method that outptuts a conclusion sentence for each of the random integers generated
  public static String conclusionSent(int randomInt){
    String conclusionSentence = "";
    switch (randomInt){
      case 0: conclusionSentence = "That fox knew her bulldozers.";
      break;
      case 1: conclusionSentence = "The fox was crazy!";
      break;
      case 2: conclusionSentence = "It had a lot of energy!";
      break;
      case 3: conclusionSentence = "The fox liked to be alone.";
      break;
      case 4: conclusionSentence = "The fox was starving!";
      break;
      case 5: conclusionSentence = "The fox was happy.";
      break;
      case 6: conclusionSentence = "This fox was sick.";
      break;
      case 7: conclusionSentence = "That fox was always starting fights!";
      break;
      case 8: conclusionSentence = "The fox was always angry.";
      break;
      case 9: conclusionSentence = "The fox was sad.";
      break;
      }
    return conclusionSentence;
  }
}