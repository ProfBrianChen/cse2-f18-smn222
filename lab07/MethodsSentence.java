//Sofia Nachmias
//CSE002 Lab 07
//November 8, 2018

import java.util.Random; //imports the Random class
import java.util.Scanner; //imports the Scanner class

public class MethodsSentence{
  //main method required for every Java program 
  public static void main(String[]args){

    Scanner myScanner = new Scanner(System.in); //creates the new scanner
    
    boolean yes = true; //declares the boolean value yes as true
    
    //Creates a loop that, at each iteration, assembles the words into a string and prints it
    while (yes){
      Random randomGenerator = new Random(); //creats the new random generator
      int randomInt = randomGenerator.nextInt(10); //generates a random integer less than 10
      
      String adjectiveWord = adjectives(randomInt); //declares the adjectiveWord variable as a string and matches it to its method
      String subjectWord = subject(randomInt); //declares the subjectWord variable as a string and matches it to its method
      String verbWord = verb(randomInt); //declares the verbWord variable as a string and matches it to its method
      String objectWord = object(randomInt); //declares the objectWord variable as a string and matches it to its method
      
      
      
      System.out.println("The " + adjectiveWord + " " + subjectWord + " " + verbWord + " the " + objectWord + "."); //prints out the sentence
    
      //asks the user if they want to continue generating sentences and accepts the users input
      System.out.println("Do you want to generate a sentence?");
      String input = myScanner.next();
      
      //checks user input 
      if (input.equals("yes")){
        yes = true;
      }
      else if (input.equals("no")){
        yes = false;
      }
      else if (input.equals(" ")){
        System.out.println("Please input yes or no.");
      }
      
      //check for yes
      if (yes) {
        
      }
      else {
        System.exit(0);
      }
    }
  }
  
  
  //method that outptuts an adjective for each of the random integers generated
  public static String adjectives(int randomInt){
    String adjectiveString = "";
    switch (randomInt) {
      case 0: adjectiveString ="brown";
      break;
      case 1: adjectiveString= "happy";
      break;
      case 2: adjectiveString = "loud";
      break;
      case 3: adjectiveString = "blue";
      break;
      case 4: adjectiveString = "angry";
      break;
      case 5: adjectiveString = "excited";
      break;
      case 6: adjectiveString = "nervous";
      break;
      case 7: adjectiveString = "big";
      break;
      case 8: adjectiveString = "small";
      break;
      case 9: adjectiveString = "healthy";
      break;
    }
    return adjectiveString;
  }
  
  //method that outptuts a subject for each of the random integers generated
  public static String subject(int randomInt){
    String subjectString = "";
    switch (randomInt) {
      case 0: subjectString ="fox";
      break;
      case 1: subjectString= "dog";
      break;
      case 2: subjectString = "girl";
      break;
      case 3: subjectString = "birds";
      break;
      case 4: subjectString = "boy";
      break;
      case 5: subjectString = "dogs";
      break;
      case 6: subjectString = "mom";
      break;
      case 7: subjectString = "man";
      break;
      case 8: subjectString = "students";
      break;
      case 9: subjectString = "family";
      break;
    }
    return subjectString;
  }

    //method that outptuts a verb for each of the random integers generated
  public static String verb(int randomInt){
    String verbString = "";
    switch (randomInt) {
      case 0: verbString ="passed";
      break;
      case 1: verbString= "walked past";
      break;
      case 2: verbString = "played with";
      break;
      case 3: verbString = "flew above";
      break;
      case 4: verbString = "hit";
      break;
      case 5: verbString = "jumped on";
      break;
      case 6: verbString = "followed";
      break;
      case 7: verbString = "was";
      break;
      case 8: verbString = "learned";
      break;
      case 9: verbString = "ate";
      break;
    }
    return verbString;
  }
    
  //method that outptuts an object for each of the random integers generated
  public static String object(int randomInt){
    String objectString = "";
    switch (randomInt) {
      case 0: objectString ="dog";
      break;
      case 1: objectString= "cat";
      break;
      case 2: objectString = "balloon";
      break;
      case 3: objectString = "trees";
      break;
      case 4: objectString = "desk";
      break;
      case 5: objectString = "girl";
      break;
      case 6: objectString = "truck";
      break;
      case 7: objectString = "teacher";
      break;
      case 8: objectString = "alphabet";
      break;
      case 9: objectString = "fruit";
      break;
    }
    return objectString;
  }      
  
  public static String action(int randomInt){
    String actionString = "";
    switch (randomInt){
      case 0: actionString = "This fox was particularly mean to passing warplanes.";
      break;
      case 1: actionString = "The fox knew her bulldozers.";
      break;
      case 2: actionString = "This fox was running in the woods.";
      break;
      case 3: actionString = "This fox was particularly mean to the other animals.";
      break;
      case 4: actionString = "The fox ate the leaves.";
      break;
      case 5: actionString = "The fox was happy.";
      break;
      case 6: actionString = "This fox spew plant matter at the cat.";
      break;
      case 7: actionString = "The fox fought the dog.";
      break;
      case 8: actionString = "The fox was always angry.";
      break;
      case 9: actionString = "The fox tried to be by itself all of the time.";
      break;
      }
return actionString;
}
}