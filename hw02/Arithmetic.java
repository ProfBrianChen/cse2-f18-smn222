// Sofia Nachmias
// September 10, 2018
//<<<<<<< HEAD
// CSE 02 - Arithmetic 
//=======
// CSE 02 - Arithmetic
//>>>>>>> 000c93b0375ca9bc44a7b5d24ed70745a3498cac

public class Arithmetic{
  // main method required for every Java program
  
  public static void main (String[] args) {
    
  //Number of pairs of pants
  int numPants = 3;
  //Cost per pair of pants
  double pantsPrice = 34.98;
  
  //Number of sweatshirts
  int numShirts = 2;
  //Cost per shirt
  double shirtPrice = 24.99;
  
  //Number of belts
  int numBelts = 1;
  //Cost per belt
  double beltCost = 33.99;
  
  //the tax rate
  double paSalesTax = 0.06;
  
  float totalCostPants = (float) (numPants * pantsPrice); //total cost of pants 
  float totalCostShirts = (float) (numShirts * shirtPrice); //total cost of shirts
  float totalCostBelts = (float) (numBelts * beltCost); //total cost of belts 
  
  float salesTaxPants = (float) (paSalesTax * totalCostPants); //sales tax charged on on pants 
  float salesTaxShirts = (float) (paSalesTax * totalCostShirts); //sales tax charged on shirts 
  float salesTaxBelts = (float) (paSalesTax * totalCostBelts); //sales tax charged on belts 
  
  float totalCostPurhcase = (float) (totalCostPants + totalCostShirts + totalCostBelts); // total cost of purchases before tax
  float totalSalesTax = (float) (salesTaxPants + salesTaxShirts + salesTaxBelts); //total sales tax
  float totalPrice = (float) (totalCostPurhcase + totalSalesTax); //total paid for transacton, including sales tax 
  
  // change total to be written with two decimal points
  String salesTaxPantsString = String.format("%.2f", salesTaxPants);
  String salesTaxShirtsString = String.format("%.2f", salesTaxShirts);
  String salesTaxBeltsString = String.format("%.2f", salesTaxBelts);
  String totalCostPurhcaseString = String.format("%.2f", totalCostPurhcase);
  String totalSalesTaxString = String.format("%.2f", totalSalesTax);
  String totalPriceString = String.format("%.2f", totalPrice);
  
    
  // print out the output data 
  System.out.println ("The tax on the purchase of pants is " + salesTaxPantsString + ".");
  System.out.println ("The tax on the purchase of shirts is " + salesTaxShirtsString + ".");
  System.out.println ("The tax on the purchase of belts is " + salesTaxBeltsString + ".");
  System.out.println("The total cost of the purchase is " + totalCostPurhcase + ".");
  System.out.println("The total sales tax is " + totalSalesTaxString + ".");
  System.out.println("The total cost of the transaction is " + totalPriceString + ".");
 
}
}
  