//Sofia Nachmias
//October 4, 2018
//CSE 002 - User Input

import java.util.Scanner; //imports the scanner class

public class UserInput {
  // main method required for every Java program
  public static void main (String [] args){
    
    Scanner myScanner = new Scanner(System.in);
    
    boolean number = false;
    boolean department = false;
    boolean classes = false;
    boolean time = false;
    boolean instructor = false;
    boolean students = false;
    
    int courseNumber = 0; // an integer corresponding to the course number 
    String departmentName = ""; // a String corresponding to the department name  
    int classesPerWeek = 0; // an integer corresponding to the number of classes per week 
    String classTime = "00:00"; // a String corresponging to the class time 
    String instructorName = ""; // a String corresponding to the name of the instructor 
    int studentsNumber = 0; // an integer corresponding to the number of students in the class
    
    //takes the users input for the course number and dispalys it 
    while (!number){
      System.out.println("What is the course number of this class?");
      number = myScanner.hasNextInt();
      if (number){
        courseNumber = myScanner.nextInt();
      System.out.println("The course number is: " + courseNumber);
      }
      else {
        myScanner.next();
      }
    }
    
    //takes the users input for the department name and dispalys it 
    while (!department){
      System.out.println("What department is the class in?");
      department = myScanner.hasNext();
      if (department){
        departmentName = myScanner.next();
      System.out.println("The department name is: " + departmentName);
      }
      else {
        myScanner.next();
      }
    }
    
    //takes the users input for the number of times the class meets per week and dispalys it 
    while (!classes){
      System.out.println("How many times per week does the class meet?");
      classes = myScanner.hasNextInt();
      if (classes){
        classesPerWeek = myScanner.nextInt();
      System.out.println("The class meets " + classesPerWeek + " times per week.");
      }
      else {
        myScanner.next();
      }
    }
    
    //takes the users input for the time the class starts and dispalys it 
    while (!time){
      System.out.println("What time does the class start?");
      time = myScanner.hasNext();
      if (time){
        classTime = myScanner.next();
      System.out.println("The class starts at: " + classTime);
      }
      else {
        myScanner.next();
      }
    }
    
    //takes the users input for the instructor's name and dispalys it 
    while (!instructor){
     System.out.println("What is the instructor's name?");
     instructor = myScanner.hasNext();
     if (instructor){
      instructorName = myScanner.next();
      System.out.println("The instructor's name is: " + instructorName);
     }
      else {
        myScanner.next();
      }
    }
    
    //takes the users input for the number of students in the class and dispalys it 
    while (!students){
      System.out.println("How many stuents are in the class?");
      students = myScanner.hasNextInt();
      if (students){
        studentsNumber = myScanner.nextInt();
        System.out.println("There are " + studentsNumber + " students in the class.");
      }
      else {
        myScanner.next();
      }
    }
    
  }
  }