//////////////
////Sofia Nachmias
////Septermber 18, 2018
////CSE 02 Convert
///

import java.util.Scanner; //imports the Scanner class

public class Convert{
  // main method required for every Java program
  public static void main (String[] args) {
    
    Scanner myScanner = new Scanner (System.in);
    
    System.out.print("Enter the affected area in acres: "); //prompts the user for the number of acres of land affected by hurricane precipitation
    double affectedAcres = myScanner.nextDouble(); //accepts user input
    System.out.print("Enter the rainfall in the affected area: "); //prompts the user for the number of inches of rain that were dropped on average
    double inchesRainfall = myScanner.nextDouble(); //accepts user input
    inchesRainfall *=  0.00058001; //allows the conversion from cubic inches to cubic miles
    System.out.println(inchesRainfall + " cubic miles."); //prints out how many cubic miles of rainfall in area
    
  }
}
   