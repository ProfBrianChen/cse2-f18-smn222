//////////////
////Sofia Nachmias
////Septermber 18, 2018
////CSE 02 Pyramid
///

import java.util.Scanner; //imports the Scanner class

public class Pyramid{
  // main method required for every Java program
  public static void main (String[] args) {
    
    Scanner myScanner = new Scanner (System.in);
    
    System.out.print("The square side of the pryramid is (input length): "); //prompts the user for the length of the pyramid
    double squareSide = myScanner.nextDouble(); //accepts user input
    System.out.print("The height of the pyramid is (input height): "); //prompts the user for the height of the pyramid
    double squareHeight = myScanner.nextDouble(); //accepts user input
    
    int pyramidVolume = (int) ((squareSide*squareSide*squareHeight)/3); //calculates the volume of the pyramid
    System.out.println("The volume inside the pyramid is: " + pyramidVolume); //displays the volume of the pyrmaid
    
  }
}
