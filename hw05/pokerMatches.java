//Sofia Nachmias
//October 9, 2018
//CSE002 - HW05

import java.util.Scanner; //imports the scanner class

public class pokerMatches{
  // main method required for every Java program
  public static void main (String[] args){
    Scanner myScanner = new Scanner(System.in);
    
  int onePairCounter = 0; //an integer corresponding to the number of One-Pairs
  int twoPairCounter = 0; //an integer corresponding to the number of Two-Pairs
  int threeOfAKindCounter = 0; //an integer corresponding to the number of Three-of-a-Kinds
  int fourOfAKindCounter = 0; //an integer corresponding to the number of Four-of-a-Kinds
  
  //takes the users input for the number of hands and dispalys it
  System.out.println("Input the number of hands to generate.");
      int numHands = myScanner.nextInt();
  
  //generates a random card for the hand
  for (int i = 1; i <= numHands; i++){
    int card1 = (int)(Math.random()*(52))+1;
    int card2 = (int)(Math.random()*(52))+1;
    int card3 = (int)(Math.random()*(52))+1;
    int card4 = (int)(Math.random()*(52))+1;
    int card5 = (int)(Math.random()*(52))+1;
 while (card2 == card1){
    card2 = (int)(Math.random()*(52))+1;
  }

//generates the card as a number between 1 and 13 for each suit
card1 = card1 % 13;
System.out.println("card 1 =  " + card1);
card2 = card2 % 13;
System.out.println("card 2 = " + card2);
card3 = card3 % 13;
System.out.println("card 3 = " + card3);
card4 = card4 % 13;
System.out.println("card 4 = " + card4);
card5 = card5 % 13;
System.out.println("card 5 = " + card5);
 
 //one-pair
 if (card1 == card2){
  onePairCounter ++;
  }

 //two-pair
 if (card1 == card2 || card1 == card3 || card1 == card4){
  twoPairCounter ++;
  }
 if (card2 == card3 || card2 == card4){
   twoPairCounter ++;
  }
 if (card3 == card4){
   twoPairCounter ++;
 }
 
 //three-of-a-kind
 if ((card1 == card2 && card2 == card3) && (card4 == card5)){
  threeOfAKindCounter ++;
  }
 if ((card1 == card2 & card2 == card4) && (card3 == card5)){
   threeOfAKindCounter ++;
  }
 if ((card1 == card2 && card2 == card5) && (card3 == card4)){
  threeOfAKindCounter ++;
  }
 if ((card1 == card3 && card3 == card2) && (card4 == card5)){
  threeOfAKindCounter ++;
  }
 if ((card1 == card3 && card3 == card4) && (card2 == card5)){
  threeOfAKindCounter ++;
  }
 if ((card1 == card3 && card3 == card5) && (card4 == card2)){
  threeOfAKindCounter ++;
  }
 if ((card1 == card4 && card4 == card2) && (card3 == card5)){
  threeOfAKindCounter ++;
  }
 if ((card1 == card4 && card4 == card3) && (card2 == card5)){
  threeOfAKindCounter ++;
  }
 if ((card1 == card4 && card4 == card5) && (card3 == card2)){
  threeOfAKindCounter ++;
  }
 
 //four-of-a-kind
 if (card1 == card2 && card1 == card3 && card1 == card4){
  fourOfAKindCounter ++;
  }
 if (card1 == card3 && card1 == card4 && card1 == card5){
  fourOfAKindCounter ++;
  }
 if (card1 == card2 && card1 == card4 && card1 == card5){
  fourOfAKindCounter ++;
  }
 if (card1 == card2 && card1 == card3 && card1 == card5){
  fourOfAKindCounter ++;
  }
}
  //prints out the statements of the number of loops and the propability of each pair occurring
  System.out.println("The number of loops: " + numHands);
  System.out.println("The probability of One-Pair: " + onePairCounter); 
  System.out.println("The probability of Two-Pair: " + twoPairCounter); 
  System.out.println("The probability of Three-of-a-kind: " + threeOfAKindCounter); 
  System.out.println("The probability of Four-of-a-kind: " + fourOfAKindCounter); 
  }
}
