//Sofia Nachmias
//September 20, 2018
//CSE 002 - Card Generator


public class CardGenerator {
  //main method required for every Java program
  public static void main (String[] args) {
    String suit = ""; //a String corresponding to the name of the suit 
    String identity = ""; //a String corresponding to the identity of the card
    
    int cardNumber = (int)(Math.random()*52)+1; //fixes the range of integers
    
    
    //assgins the randomly generated card to a suit name 
    if (cardNumber<=13 && cardNumber>1) {
      suit = "Diamonds";
    }
    else if (cardNumber<=26 && cardNumber>14) {
      suit = "Clubs";
    }
    else if (cardNumber<=39 && cardNumber>27) {
      suit = "Hearts";
    }
    else if (cardNumber<=52 && cardNumber>40) {
      suit = "Spades";
    }
    
    int card = cardNumber%13;
    //assigns the randomly generated card with a card identity using a switch
    //System.out.println("Card number = " + card);
    switch (card) {
      case 1: identity = "Ace";
        break;
      case 2: identity = "2";
        break;
      case 3: identity = "3";
        break;
      case 4: identity = "4";
        break;
      case 5: identity = "5";
        break;
      case 6: identity = "6";
         break;
      case 7: identity = "7";
        break;
      case 8: identity = "8";
        break;
      case 9: identity = "9";
        break;
      case 10: identity = "10";
        break;
      case 11: identity = "Jack";
        break;
      case 12: identity = "Queen";
        break;
      case 0: identity = "King";
        break;
      default:
        System.out.println("Invalid Card");
        break;
    }
    
    System.out.println("You picked the " + identity + " of " + suit + "."); //prints out the name of the randomly selected card
    
    } 
  }